declare global {
    namespace NodeJS {
        interface ProcessEnv {
            TOKEN: string;
            TEST_GUILD_ID: string;
        }
    }
}

export {};