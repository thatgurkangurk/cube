import { Awaitable, ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { ExtendedClient } from "../structures/Client";
import { LoggerFunction } from "./Module";

type CommandExec = (props: CommandProps) => Awaitable<unknown>;

interface CommandProps {
    interaction: ChatInputCommandInteraction;
    client: ExtendedClient;
    log: LoggerFunction;
}

type CommandMeta =
    | SlashCommandBuilder
    | Omit<SlashCommandBuilder, "addSubcommand" | "addSubcommandGroup">

interface Command {
    meta: CommandMeta;
    exec: CommandExec;
}

export {
    CommandProps,
    CommandMeta,
    CommandExec,
    Command
}