import { ClientEvents, Awaitable } from "discord.js";
import { ExtendedClient } from "../structures/Client";
import { LoggerFunction } from "./Module";

interface EventProps {
    client: ExtendedClient
    log: LoggerFunction
}

type EventKeys = keyof ClientEvents;

type EventExec<T extends EventKeys> =
    (props: EventProps, ...args: ClientEvents[T]) => Awaitable<unknown>;

interface Event<T extends EventKeys> {
    id: T
    exec: EventExec<T>
}

export {
    EventProps,
    EventKeys,
    EventExec,
    Event
}