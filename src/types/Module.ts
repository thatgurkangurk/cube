import { Command } from "./Command";
import { Event } from "./Event";

enum ModuleType {
    "command",
    "event"
}

interface Module {
    type: ModuleType;
    command?: Command;
    event?: Event<any>;
}

type LoggerFunction = (...args: unknown[]) => void;

export {
    Module,
    ModuleType,
    LoggerFunction
}