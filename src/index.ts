import 'dotenv/config';
import { modules } from './modules';
import { ExtendedClient } from "./structures/Client";

export const client: ExtendedClient = new ExtendedClient({
    intents: ["Guilds"]
});

client.start(process.env.TOKEN, modules);