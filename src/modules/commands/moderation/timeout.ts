import { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder, GuildMember } from "discord.js";
import { command } from "../../../structures/Command";
import ms from 'ms';

const meta = new SlashCommandBuilder()
    .setName('timeout')
    .setDescription('Timeout a member')
    .setDefaultMemberPermissions(PermissionFlagsBits.ModerateMembers)
    .setDMPermission(false)
    .addUserOption((option) =>
        option
            .setName('target')
            .setDescription('Select the target member')
            .setRequired(true)
    )
    .addStringOption((option) =>
        option
            .setName('duration')
            .setDescription('Provide a duration for this timeout [1m,1h,1d]')
            .setRequired(true)
    )
    .addStringOption((option) =>
        option
            .setName('reason')
            .setDescription('Provide a reason for this timeout')
            .setMaxLength(512)
    )

export const timeoutCommand = command(meta, async ({ interaction }) => {
    const { options } = interaction;
    const member = interaction.member as GuildMember;

    const target = options.getMember('target') as GuildMember;
    const duration = options.getString('duration');
    const reason = options.getString('reason') || "No reason specified";

    const errorsArray: string[] = [];

    const errorsEmbed = new EmbedBuilder()
        .setAuthor({ name: 'Could not timeout member due to' })
        .setColor('Red');

    if (!target) return interaction.reply({
        embeds: [errorsEmbed.setDescription("Member has most likely left the guild")],
        ephemeral: true,
    });

    if (!ms(duration) || ms(duration) > ms("28d")) errorsArray.push("Time provided is invalid or over the 28d limit");

    if (!target.manageable || !target.moderatable) errorsArray.push("Selected target is not moderatable by this bot.");

    if (member.roles.highest.position < target.roles.highest.position) errorsArray.push("Selected member has a higher role position than you.");

    if (errorsArray.length) return interaction.reply({
        embeds: [errorsEmbed.setDescription(errorsArray.join("\n"))],
        ephemeral: true,
    });

    target.timeout(ms(duration), reason).catch((err) => {
        interaction.reply({
            embeds: [errorsEmbed.setDescription("Could not timeout user due to a very uncommon error.").addFields(err)],
            ephemeral: true,
        })
        return console.error("Error happened in timeout.ts", err)
    });

    const successEmbed = new EmbedBuilder().setColor('Green').setAuthor({ name: `Issued by ${member.user.tag}` }).setTimestamp().setTitle('Success!').setDescription(`Successfully timed out <@${target.user.id}> for ${duration} with the reason "${reason}"`);

    return interaction.reply({
        embeds: [successEmbed],
        ephemeral: true,
    })
})