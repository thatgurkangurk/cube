import { SlashCommandBuilder } from "discord.js";
import { command } from "../../../structures/Command";

const meta = new SlashCommandBuilder()
    .setName('ping')
    .setDescription('Ping the bot for a response')
    .addStringOption((option) =>
        option
            .setName('message')
            .setDescription('Provide the bot a message to reply with')
            .setMinLength(1)
            .setMaxLength(128)
            .setRequired(false)
    );

export const pingCommand = command(meta, ({ interaction }) => {
    const message = interaction.options.getString('message');

    return interaction.reply({
        ephemeral: true,
        content: message ?? 'Pong! 🏓'
    });
})