import { EditReply, Reply } from "../../structures/Replies";
import { event } from '../../structures/Event';
import { allCommandsMap } from "..";

export const interactionCreate = event('interactionCreate', async (
    {
      log,
      client,
    },
    interaction,
  ) => {
    if (!interaction.isChatInputCommand()) return;
  
    try {
      const commandName = interaction.commandName
      const command = allCommandsMap.get(commandName)
  
      if (!command) throw new Error('Command not found...')
  
      await command.exec({
        client,
        interaction,
        log(...args) {
          log(`[${command.meta.name}]`, ...args)
        },
      })
    } catch (error) {
      log('[Command Error]', error)
  
      if (interaction.deferred)
        return interaction.editReply(
          EditReply.error('Something went wrong :(')
        )
  
      return interaction.reply(
        Reply.error('Something went wrong :(')
      )
    }
});