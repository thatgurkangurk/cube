import { event } from "../../structures/Event";

export const readyEvent = event('ready', ({ log }, client) => {
    log(`Logged in as ${client.user.tag}`);
})