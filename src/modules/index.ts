import { Command } from "../types/Command";
import { Module } from "../types/Module";
import { pingCommand } from "./commands/info/ping";
import { timeoutCommand } from "./commands/moderation/timeout";
import { interactionCreate } from "./events/interactionCreate";
import { readyEvent } from "./events/ready";

const modules: Module[] = [
    timeoutCommand,
    pingCommand,
    readyEvent,
    interactionCreate
];

const allModules = modules.map(module => module).flat();
const allCommandsMap = new Map<string, Command>(
    allModules.map((module) => [module.command?.meta.name, module.command])
);

export {
    modules,
    allCommandsMap
}