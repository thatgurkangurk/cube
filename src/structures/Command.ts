import {
    CommandExec,
    CommandMeta
} from '../types/Command';
import { Module, ModuleType } from '../types/Module';

export function command(meta: CommandMeta, exec: CommandExec): Module {
    const module: Module = {
        type: ModuleType.command,
        command: {
            meta,
            exec
        }
    }
    return module;
}