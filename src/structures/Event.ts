import { Event, EventExec, EventKeys } from "../types/Event";
import { Module, ModuleType } from "../types/Module";
import { ExtendedClient } from "./Client";

export function event<T extends EventKeys>(id: T, exec: EventExec<T>): Module {
    const module: Module = {
        type: ModuleType.event,
        event: {
            id,
            exec
        }
    }

    return module;
}

export function registerEvent(client: ExtendedClient, event: Event<any>): void {
    client.on(event.id, async (...args) => {
        const props = {
            client,
            log: (...args: unknown[]) =>
                console.log(`[${event.id}]`, ...args)
        }

        try {
            await event.exec(props, ...args);
        } catch (error) {
            props.log('Uncaught error', error);
        }
    })
}