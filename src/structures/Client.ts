import { ApplicationCommandDataResolvable, Client, ClientOptions, Collection } from 'discord.js';
import { RegisterCommandsOptions } from '../types/Client';
import { Command } from '../types/Command';
import { Module, ModuleType } from '../types/Module';
import { registerEvent } from './Event';

class ExtendedClient extends Client {
    commands: Collection<string, Command> = new Collection();

    constructor(options: ClientOptions) {
        super(options);
    }

    start(token: string, modules: Module[]) {
        this.registerModules(modules);
        this.login(token);
    }

    async registerCommands({ commands, guildID }: RegisterCommandsOptions) {
        if (guildID) {
            this.guilds.cache.get(guildID)?.commands.set(commands);
            console.log(`Registerring commands to ${guildID}`);
        } else {
            this.application?.commands.set(commands);
            console.log('Registerring global commands');
        }
    }

    async registerModules(modules: Module[]) {
        const slashCommands: ApplicationCommandDataResolvable[] = [];
        modules.forEach(async module => {
            switch(module.type) {
                case ModuleType.command:
                    this.commands.set(module.command.meta.name, module.command);
                    slashCommands.push(module.command.meta);
                    break;
                case ModuleType.event:
                    registerEvent(this, module.event);
            }
        })


        this.once('ready', () => {
            this.registerCommands({
                commands: slashCommands,
                guildID: process.env.TEST_GUILD_ID
            })
        })
    }
}

export {
    ExtendedClient
};